#include <stdio.h>

unsigned char file[100000000];

typedef struct Memory Memory;
struct Memory{
  unsigned int offset;
  unsigned int addr;
  unsigned int section; // 1 = text, 2 = data, 3 = bss

  unsigned char code;
};

unsigned int offset_text[6];
unsigned int offset_data[10];


unsigned int addr_text[6];
unsigned int addr_data[10];

unsigned int size_text[6];
unsigned int size_data[10];

unsigned int addr_bss;
unsigned int size_bss;

unsigned int entry_point;

Memory memory[100000000];
unsigned int PC;
unsigned int address;
unsigned int mem_pos;

unsigned int hex_merge(unsigned int from);
Memory mem(Memory *memr, unsigned int vaddr, unsigned int mem_pos);

  
int main()
{
  
  FILE *fp;
  int byte;
  int count=0;
  int c=1;
  
  char file_name[16];
  scanf("%s", &file_name);
  fp = fopen(file_name, "rb");
  
  while ((byte = getc(fp)) != EOF)
    {
      file[count] = byte;
      count++;
    }

  entry_point = hex_merge(0xE0);
  
  size_bss = hex_merge(0xDC);
  addr_bss = hex_merge(0xD8);
  printf("BSS %x\n", addr_bss);
  
  
  offset_text[0] = hex_merge(0);
  
  while (c!=7)
    {
      offset_text[c] = hex_merge(c*4);
      c+=1;
    }

  offset_data[0] = hex_merge(0x1C);

  c=1;

  while(c!=11)
    {
      
      offset_data[c] = hex_merge( (0x20+c*4)-4 );
      c+=1;
    }

  addr_text[0] = hex_merge(0x48);

  c=1;

  while(c!=7)
    {
     
      addr_text[c] = hex_merge( (0x4C+c*4)-4 );
      printf("TEXT%d %x\n", c-1, addr_text[c-1]);
      c+=1;
    }

  c=1;

  addr_data[0] = hex_merge(0x64);

  while(c!=11)
    {
      
      addr_data[c] = hex_merge((0x68+c*4)-4);
      printf("DATA%d %x\n", c-1, addr_data[c-1]);
      c+=1;
    }

  size_text[0] = hex_merge(0x90);
  c=1;
  while(c!=7)
    {
      size_text[c] = hex_merge((0x94+c*4) -4);
      c++;
    }

  size_data[0] = hex_merge(0xAC);
  c=1;


  while(c!=11)
    {
      size_data[c] = hex_merge((0xB0+c*4) -4);
      c++;
    }
    
  int texthsb=0;
  int hsb;

  while (texthsb < 7){
   while (hsb < size_text[texthsb])
     {
       memory[mem_pos].code = (unsigned char)(file[offset_text[texthsb]+hsb]);
       memory[mem_pos].offset = offset_text[texthsb]+hsb;
       memory[mem_pos].addr = addr_text[texthsb]+hsb;

       memory[mem_pos].section = 1;

       mem_pos++;
       hsb++;
    }

   texthsb++;
  }

  int datahsb=0;
  int hsb2;
  while (datahsb < 10){
   while (hsb2 < size_data[datahsb])
     {
       memory[mem_pos].code = (unsigned char)(file[offset_data[datahsb]+hsb2]);
       memory[mem_pos].offset = offset_data[datahsb]+hsb2;
       memory[mem_pos].addr = addr_data[datahsb]+hsb2;

       memory[mem_pos].section = 2;

       mem_pos++;
       hsb2++;

    }

   datahsb++;
  }

  int hsb3;
  while (hsb3 < size_bss)
    {
      memory[mem_pos].addr = addr_bss+hsb3;

      memory[mem_pos].section = 3;

      mem_pos++;
      hsb3++;
    }


  printf("Entry point : %x\n", entry_point);
  Memory ahyana;
 
  ahyana = mem(memory, entry_point, mem_pos);
  printf("AHYANA %x\n", ahyana.offset);
  ahyana = mem(memory, addr_text[0]+4, mem_pos);
  printf("EHL %x\n", ahyana.addr);
 
}

unsigned int hex_merge(unsigned int from)
{
  unsigned int hex;
  hex = (unsigned int)(file[from]<<24 | file[from+1]<<16 | file[from+2]<<8 | file[from+3]<<0);
  return hex;
}

Memory mem(Memory memr[], unsigned int vaddr, unsigned int mem_pos)
{
  int h;
  int bool;
  while(h != mem_pos)
    {      
      if (memr[h].addr == vaddr){
        return memr[h];
        break;
      }
      h++;
      
    }
  
}
